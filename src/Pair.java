/**
 *
 * @author adam
 */
public class Pair {
    private final String host;
    private final int port;
    
    public Pair(String host, int port) {
        this.host = host;
        this.port = port;
    }
    
    public String getHost() {
        return this.host;
    }
    
    public int getPort() {
        return this.port;
    }
}
