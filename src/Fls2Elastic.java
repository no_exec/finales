import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.regex.Pattern;
import org.apache.tika.detect.DefaultDetector;
import org.apache.tika.detect.Detector;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.sax.BodyContentHandler;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.elasticsearch.ElasticsearchException;
import org.xml.sax.SAXException;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 *
 * @author adam
 */
public class Fls2Elastic {

    public static final int MAX_FILE_SIZE = 3000000;

    // logging directory
    public static final String logDir = "/tmp/";

    /**
     * @param args the command line arguments
     * args[0]: body file (output from sleuthkit's fls)
     * args[1]: file system mount point that will be indexed
     * args[2]: name of ElasticSearch cluster
     * args[3]: index name of cluster
     * args[4]: host where ElasticSearch is running (e.g. localhost)
     * args[5]: port for node communication (e.g. 9300)
     */
    public static void main(String[] args) {
        long start = System.nanoTime();
        if ((args.length < 6) || (args.length % 2 != 0)) {
            System.err.println("Invalid arguments");
            System.out.println("Usage: Fls2Elastic body_file mount_point cluster_name index_name host port");
            System.out.println("body_file - path to body file (output from fls)");
            System.out.println("mount_point - mount point of filesystem to be processed");
            System.out.println("cluster_name - cluster name used by ElasticSearch");
            System.out.println("index_name - index name to be used");
            System.out.println("host - host where ElasticSearch is running");
            System.out.println("port - port for node communication");
            System.exit(-1);
        }
        String flsFileName = args[0];
        String mountPoint = args[1];
        String clusterName = args[2];
        String indexName = args[3];
        int numOfHosts = (args.length - 4)/2;
        Pair[] hosts = new Pair[numOfHosts];
        for (int i = 0; i < numOfHosts; i++) {
            hosts[i] = new Pair(args[4+2*i],Integer.parseInt(args[4+2*i+1]));
        }
        
        Settings settings = Settings.builder().put("cluster.name", clusterName).put("client.transport.sniff", true).build();
        TransportClient client = new PreBuiltTransportClient(settings);
        for (Pair host : hosts) {
            try {
				client.addTransportAddress(new TransportAddress(InetAddress.getByName(host.getHost()), host.getPort()));
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
        }
        Date now = new Date();
        SimpleDateFormat form = new SimpleDateFormat("yy-MM-dd'T'HH-mm-ssZ");
        Logger logger = Logger.getLogger(Fls2Elastic.class.getName());
        File logs = new File(logDir + "logs");
        logs.mkdir();
        File logging = new File(logDir + "logs" + File.separator + form.format(now) + ".log");
        FileHandler fh = null;
        try {
            fh = new FileHandler(logging.getPath());
        } catch (IOException | SecurityException ex) {
            logger.log(Level.SEVERE, null, ex);
            System.exit(-1);
        }
        SimpleFormatter formatter = new SimpleFormatter();  
        fh.setFormatter(formatter);
        logger.addHandler(fh);
        logger.log(Level.INFO, "Program started!");
        BufferedReader fls = null;
        BufferedReader line_num = null;
        File temp = null;
        RandomAccessFile contents = null;
        try {
            fls = new BufferedReader(new FileReader(flsFileName));
            line_num = new BufferedReader(new FileReader(flsFileName));
            temp = File.createTempFile("contents_buffer", ".tmp");
            contents = new RandomAccessFile(temp, "rw");
        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, null, ex);
            System.exit(-1);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
            System.exit(-1);
        }
        String line, md5, filePath;
        int parts, lastBlock = 0, num, i, lines = 0,
            count = 0, end = 100, progress = 0, prev_prog = 0;
        byte[] part;
        String path, fileType = "", content = "", metaDataStr = "", partStr;
        StringBuilder entryBuilder = new StringBuilder(100);
        IndexResponse response;
        Date date = new Date();
        DateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss Z");
        String atime, mtime, ctime, crtime, h_atime, h_mtime, h_ctime, h_crtime;
        try {
        while (line_num.readLine() != null) lines++;
        line_num.close();
        while ((line = fls.readLine()) != null) {
            count++;
            prev_prog = progress;
            progress = (int)(((float)count/(float)lines)*100);
            if (progress > prev_prog && progress <= end) {
                System.out.print("|");
                for (int j = 0; j < progress/3; j++) {
                    System.out.print("#");
                }
                for (int j = progress/3; j < end/3; j++) {
                    System.out.print(" ");
                }
                System.out.print("|" + progress + "%\r");
            }
            int startPos = 0;
            String[] tokens = line.split(Pattern.quote("|"));
            if (tokens.length != 11) {
                md5 = tokens[0];
                filePath = "";
                startPos = tokens.length - 11;
                for (int j = 0; j < startPos; j++) {
                    filePath += tokens[j + 1] + "|";
                }
            } else {
                filePath = tokens[1];
                md5 = tokens[0];
            }
            atime = tokens[startPos + 7];
            mtime = tokens[startPos + 8];
            ctime = tokens[startPos + 9];
            crtime = tokens[startPos + 10];
            if (!atime.equals("0")) {
                atime += "000";
            }
            if (!mtime.equals("0")) {
                mtime += "000";
            }
            if (!ctime.equals("0")) {
                ctime += "000";
            }
            if (!crtime.equals("0")) {
                crtime += "000";
            }
            date.setTime(Long.parseLong(atime));
            h_atime = format.format(date);
            date.setTime(Long.parseLong(mtime));
            h_mtime = format.format(date);
            date.setTime(Long.parseLong(ctime));
            h_ctime = format.format(date);
            date.setTime(Long.parseLong(crtime));
            h_crtime = format.format(date);

            if (mountPoint.endsWith("/")) {
                path = mountPoint + filePath.substring(1, filePath.length());
            }
            else {
                path = mountPoint + filePath;
            }
            File file = new File(path);
            num = 1;
            parts = 1;
            i = 0;
            boolean parted = false, written = false;
            try {
                if (!file.exists()) {
                    fileType = "UNKNOWN";
                    content = "UNKNOWN";
                    metaDataStr = "UNKNOWN";
                } else {
                    if (file.isDirectory()) {
                        fileType = "directory";
                    } else if (!file.isFile()) {
                        fileType = "NOT_A_FILE";
                        content = "UNKNOWN";
                        metaDataStr = "UNKNOWN";
                    } else {
                        if (path.contains("(deleted")) {
                            fileType = "UNKNOWN";
                            content = "DELETED";
                            metaDataStr = "DELETED";
                        } else {
                                Metadata metadata = new Metadata();
                                metadata.set(Metadata.RESOURCE_NAME_KEY, file.toString());
                                Detector detector = new DefaultDetector();
                                InputStream stream = new FileInputStream(file);
                                BufferedInputStream input = new BufferedInputStream(stream);
                                MediaType mimetype = detector.detect(input, metadata);
                                fileType = mimetype.getType() + "/" + mimetype.getSubtype();
                                AutoDetectParser parser = new AutoDetectParser();
                                BodyContentHandler handler = new BodyContentHandler(Integer.MAX_VALUE);
                                parser.parse(input, handler, metadata);
                                byte[] cont = handler.toString().getBytes();
                                int len = cont.length;
                                if (len > MAX_FILE_SIZE) {
                                    parted = true;
                                    lastBlock = len % MAX_FILE_SIZE;
                                    parts = len / MAX_FILE_SIZE + 1;
                                    if (lastBlock == 0) {
                                        parts--;
                                    }
                                    contents.seek(0);
                                    contents.write(cont);
                                }
                                while (parts > 0) {
                                    if (parted) {
                                        contents.seek(i * MAX_FILE_SIZE);
                                        int blockSize = (parts == 1) ? lastBlock : MAX_FILE_SIZE;
                                        part = new byte[blockSize];
                                        contents.read(part, 0, blockSize);
                                    } else {
                                        part = cont;
                                    }
                                    partStr = new String(part);
                                    partStr = partStr.replaceAll("\\p{Cc}", " ").replace("\\", "").replace("\"", "\\\"");
                                    if (!metadata.toString().equals("")) {
                                        metaDataStr = metadata.toString().replaceAll("\\p{Cc}", " ").replace("\\", "").replace("\"", "\\\"");
                                    }
                                    entryBuilder.delete(0, entryBuilder.length());
                                    entryBuilder.append("{")
                                            .append("\"name\" : \"").append(filePath).append("\", ")
                                            .append("\"md5\" : \"").append(md5).append("\", ")
                                            .append("\"inode\" : \"").append(tokens[startPos + 2]).append("\", ")
                                            .append("\"mode\" : \"").append(tokens[startPos + 3]).append("\", ")
                                            .append("\"uid\" : ").append(tokens[startPos + 4]).append(", ")
                                            .append("\"gid\" : ").append(tokens[startPos + 5]).append(", ")
                                            .append("\"access_time\" : ").append(atime).append(", ")
                                            .append("\"access_time_human\" : \"").append(h_atime).append("\", ")
                                            .append("\"modify_time\" : ").append(mtime).append(", ")
                                            .append("\"modify_time_human\" : \"").append(h_mtime).append("\", ")
                                            .append("\"change_time\" : ").append(ctime).append(", ")
                                            .append("\"change_time_human\" : \"").append(h_ctime).append("\", ")
                                            .append("\"creation_time\" : ").append(crtime).append(", ")
                                            .append("\"creation_time_human\" : \"").append(h_crtime).append("\", ")
                                            .append("\"size\" : ").append(tokens[startPos + 6]).append(", ")
                                            .append("\"file_type\" : \"").append(fileType).append("\", ")
                                            .append("\"metadata\" : \"").append(metaDataStr).append("\", ")
                                            .append("\"content\" : \"").append(partStr).append("\", ")
                                            .append("\"part\" : ").append(num)
                                            .append("}");

                                    response = client.prepareIndex(indexName, "info").setSource(entryBuilder.toString(), XContentType.JSON).get();

                                    written = true;
                                    parts--;
                                    num++;
                                    i++;
                                }
                            
                        }
                    }
                }
	        } catch (IOException | SAXException | TikaException | ElasticsearchException ex) {
	            logger.log(Level.SEVERE, filePath, ex);
	            if (fileType.equals("")) {
	                fileType = "UNKNOWN";
	            }
	            if (content.equals("")) {
	                content = "UNKNOWN";
	            }
                
            } finally {
                if (written) {
                    continue;
                }
                entryBuilder.delete(0, entryBuilder.length());
                entryBuilder.append("{")
                        .append("\"name\" : \"").append(filePath).append("\", ")
                        .append("\"md5\" : \"").append(md5).append("\", ")
                        .append("\"inode\" : \"").append(tokens[startPos + 2]).append("\", ")
                        .append("\"mode\" : \"").append(tokens[startPos + 3]).append("\", ")
                        .append("\"uid\" : ").append(tokens[startPos + 4]).append(", ")
                        .append("\"gid\" : ").append(tokens[startPos + 5]).append(", ")
                        .append("\"access_time\" : ").append(atime).append(", ")
                        .append("\"access_time_human\" : \"").append(h_atime).append("\", ")
                        .append("\"modify_time\" : ").append(mtime).append(", ")
                        .append("\"modify_time_human\" : \"").append(h_mtime).append("\", ")
                        .append("\"change_time\" : ").append(ctime).append(", ")
                        .append("\"change_time_human\" : \"").append(h_ctime).append("\", ")
                        .append("\"creation_time\" : ").append(crtime).append(", ")
                        .append("\"creation_time_human\" : \"").append(h_crtime).append("\", ")
                        .append("\"size\" : ").append(tokens[startPos + 6]).append(", ")
                        .append("\"file_type\" : \"").append(fileType).append("\", ")
                        .append("\"metadata\" : \"").append(metaDataStr).append("\", ")
                        .append("\"content\" : \"").append(content).append("\", ")
                        .append("\"part\" : ").append(num)
                        .append("}");

                response = client.prepareIndex(indexName, "info").setSource(entryBuilder.toString(), XContentType.JSON).get();
                
            }
        }
        long elapsed = System.nanoTime() - start;
        logger.log(Level.INFO,"End. Runtime in ms: " + elapsed / 1000000);
        System.out.println("");
        fls.close();
        client.close();
        contents.close();
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
            System.exit(-1);
        }
        temp.delete();
    }
}
