# finales

FinalES is used for analysis of a filesystem. It indexes the content of all
files located on the filesystem together with other information using Tika.
All gathered data from filesystem are loaded into ElasticSearch database.

## Gathered data

* Access time - time of last access to the file
* Modify time - time of last modification of the file
* Change time - time of last status change of the file (Unix systems)
* Creation time - time when the file was created (Windows systems)
* File type - type of the file, e. g., plain text, MS Word document, image/jpeg, audio/mpeg, etc.
* Content - text extraction of the file
* Metadata - information about the document specific to its file type, e. g. author, title, duration, etc.
* File name - name of the file
* Permission mode - permissions for operations with file
* File size - size of file in bytes
* UID - UID of the file’s owner
* GID - GID of the file’s owner
* Inode - number of index node of the file
* MD5 - MD5 checksum of the file

Program splits the large documents into 3 MB chunks, so it doesn’t cause
the web browser to stutter.


